FROM nginx
LABEL name="unfortunate-koi-react"
LABEL version="1.0" 
COPY  ./build/ /usr/share/nginx/html/
COPY ./unfortunate-koi-react.conf /etc/nginx/conf.d/
EXPOSE 80