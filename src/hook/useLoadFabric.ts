/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useLayoutEffect, useRef, useState } from "react";
declare const window: any;
let movetimeout: NodeJS.Timeout;
let changetimeout: NodeJS.Timeout;
const deepClone = (obj: any) => JSON.parse(JSON.stringify(obj));
function LoadScript(options: {
  id: string;
  src: string;
  onload?: () => void;
  onerror?: () => void;
}) {
  const el = document.getElementById(options.id);
  if (el) {
    return;
  }
  const script = document.createElement("script");
  if (options.onload) {
    script.onload = options.onload;
  }
  if (options.onerror) {
    script.onerror = options.onerror;
  }
  script.src = options.src;
  document.body.appendChild(script);
}
interface BaseProp {
  dirty: boolean;
  height: number;
  selectable: boolean;
  top: number;
  width: number;
  stroke: string;
  left: number;
  fill: string;
}
interface NodeCircle extends BaseProp {
  cleId: string;
  hasBorders: boolean;
  hasControls: boolean;
  radius: number;
}
interface Line extends BaseProp {
  dirty: boolean;
  evented: boolean;
  strokeWidth: number;
  x1: number;
  x2: number;
  y1: number;
  y2: number;
  set(params: { x1?: number; y1?: number; x2: number; y2: number }): void;
}
interface NodePositions {
  [point: string]: NodePosition & { id: string };
}
interface NodePosition {
  x: number;
  y: number;
}
interface Polygon extends BaseProp {
  id: number;
  points: NodePosition[];
  prevPositions: NodePosition[];
  prevOffset: { left: number; top: number };
  left: number;
  top: number;
}
interface CanvasState {
  nodeStack: NodeCircle[];
  nodePositions: NodePositions;
  lineStack: Line[];
  taskStack: {
    nodeStack: NodeCircle[];
    nodePositions: NodePositions;
    lineStack: Line[];
  }[];
  polygonStack: Polygon[];
  operateStack: {
    type: string;
    prevPositions?: NodePosition[];
    id: number;
    positions?: NodePosition[];
    prevOffset?: { left: number; top: number };
  }[];
}
export interface DetectAreaItem {
  areaIndex: number;
  areaPoint: { point: number[] }[];
  areaDirection: string;
  targetType: string;
  areaType: number;
}
const getInitCanvasState = () => {
  const initState: CanvasState = {
    nodeStack: [],
    nodePositions: {},
    lineStack: [],
    taskStack: [],
    polygonStack: [],
    operateStack: [],
  };
  return initState;
};
let canvasState: CanvasState | null = null;
enum OperateType {
  AddNewPolygon = "addnewpolygon",
  AddNewLine = "addnewline",
  ChangePolygon = "changepolygon",
  DeletePolygon = "deletepolygon",
  MovePolygon = "movePolygon",
}
const moveObject = (
  canvasInstance: any,
  target: any,
  { left, top }: { left: number; top: number }
) => {
  target.set("left", left);
  target.set("top", top);
  canvasInstance.current.setActiveObject(target);
  canvasInstance.current.renderAll();
};
const eventMap = {
  "mouse:move": function (canvasInstance: any) {
    return function (e: any) {
      if (!canvasState) throw new Error("未知情况");
      if (!canvasState.nodeStack.length) {
        return;
      }
      const lastline = canvasState.lineStack.slice(-1)[0];
      lastline && lastline.set({ x2: e.pointer.x, y2: e.pointer.y });
      canvasInstance.current.renderAll();
    };
  },
  "mouse:up:before": function (canvasInstance: any, fabricInstance: any) {
    return function (e: any) {
      if (!canvasState) throw new Error("未知情况");
      if (!canvasState.nodeStack.length) {
        return;
      }
      if (e.target && e.target.cleId === findNode("first")?.id) {
        // 闭合图形
        const lastline = canvasState.lineStack.slice(-1)[0];
        const firstNode =
          canvasState.lineStack.length >= 10 ? findNode("first") : e.target;
        if (canvasState.lineStack.length >= 10) {
          console.log("每个区域最多支持设置10个点");
        }
        lastline && lastline.set({ x2: firstNode.left, y2: firstNode.top });
        canvasState.taskStack.push({
          nodePositions: deepClone(canvasState.nodePositions),
          lineStack: canvasState.lineStack.slice(),
          nodeStack: canvasState.nodeStack.slice(),
        });
        const pointArr = Object.values(canvasState.nodePositions).map(
          (item: NodePosition) => ({
            x: item.x,
            y: item.y,
          })
        );
        // 初始化canvas
        canvasState.nodePositions = {};
        canvasState.lineStack = [];
        canvasState.nodeStack = [];
        canvasInstance.current.clear();
        //
        const { createPolygon } = makePolygon(fabricInstance, canvasInstance);
        const currentPolygon = createPolygon(
          pointArr,
          canvasState.polygonStack.length
        );
        currentPolygon.prevPositions = pointArr.slice();
        currentPolygon.prevOffset = {
          left: currentPolygon.left,
          top: currentPolygon.top,
        };
        console.log("---beforeGraphChange", currentPolygon.points.slice());
        canvasState.polygonStack.push(currentPolygon);
        canvasState.operateStack.push({
          type: OperateType.AddNewPolygon,
          id: canvasState.taskStack.length - 1,
          positions: currentPolygon.points.slice(),
        });
        canvasState.polygonStack.forEach((object: any) => {
          object.set("selectable", true);
        });
        canvasInstance.current.add(...canvasState.polygonStack);
      }
    };
  },
  "mouse:up": function (canvasInstance: any, fabricInstance?: any) {
    return function (e: any) {
      if (!canvasState) throw new Error("未知情况");
      if (e.transform || e.target) return;
      if (canvasState.polygonStack.length >= 5) {
        console.log("允许设置的区域最大个数为5个");
        return;
      }
      if (checkLineHasIntersect(canvasState.lineStack)) {
        console.log("---------线相交了");
        return;
      }
      const currentPoint = {
        id: `node_${canvasState.nodeStack.length}`,
        x: e.absolutePointer.x,
        y: e.absolutePointer.y,
      };
      const currentLine = makeLine(fabricInstance.current, {
        startX: currentPoint.x,
        endX: currentPoint.x,
        startY: currentPoint.y,
        endY: currentPoint.y,
      });
      canvasState.nodePositions[`node_${canvasState.nodeStack.length}`] =
        currentPoint;
      const currentCle = makeCircle(
        fabricInstance.current,
        currentPoint.x,
        currentPoint.y,
        `node_${canvasState.nodeStack.length}`
      );
      canvasState.operateStack.push({
        type: OperateType.AddNewLine,
        id: canvasState.nodeStack.length,
      });
      canvasInstance.current.add(currentLine, currentCle);
      canvasState.lineStack.push(currentLine);
      canvasState.nodeStack.push(currentCle);
      if (canvasState?.lineStack?.length) {
        canvasState.polygonStack.forEach((object: any) => {
          object.set('selectable', false);
        });
      }
    };
  },
  "object:moving": function () {
    return function (e: any) {
      const object = e.target;
      if (object.currentHeight > object.canvas.height || object.currentWidth > object.canvas.width) {
        return;
      }
      object.setCoords();
      if (object.getBoundingRect().top < 0 || object.getBoundingRect().left < 0) {
        object.top = Math.max(object.top, object.top - object.getBoundingRect().top);
        object.left = Math.max(object.left, object.left - object.getBoundingRect().left);
      }
      if (
        object.getBoundingRect().top + object.getBoundingRect().height > object.canvas.height ||
        object.getBoundingRect().left + object.getBoundingRect().width > object.canvas.width
      ) {
        object.top = Math.min(
          object.top,
          object.canvas.height - object.getBoundingRect().height + object.top - object.getBoundingRect().top,
        );
        object.left = Math.min(
          object.left,
          object.canvas.width - object.getBoundingRect().width + object.left - object.getBoundingRect().left,
        );
      }
      clearTimeout(movetimeout);
      movetimeout = setTimeout(() => {
        if (!canvasState) throw new Error("未知情况");
        canvasState.operateStack.push({
          type: OperateType.MovePolygon,
          id: object.id,
          prevOffset: { ...object.prevOffset },
        });
        object.prevOffset = { left: object.left, top: object.top };
      }, 500);
    };
  },
};
function findNode(key: string) {
  if (!canvasState) throw new Error("未知情况");

  switch (key) {
    case "first":
      key = canvasState.nodeStack[0].cleId;
      break;
    case "last":
      key = canvasState.nodeStack[canvasState.nodeStack.length - 1].cleId;
      break;
  }
  return canvasState.nodePositions[key];
}
function makeCircle(
  fabricInstance: any,
  left: number,
  top: number,
  id: string
) {
  const circle = new fabricInstance.Circle({
    left: left,
    top: top,
    // strokeWidth: 3,
    radius: 6,
    fill: "#fff",
    stroke: "#006eff",
    selectable: false,
    originX: "center",
    originY: "center",
  });
  circle.hasControls = false;
  circle.hasBorders = false;
  circle.cleId = id;
  return circle;
}

function makeLine(
  fabricInstance: any,
  {
    startX,
    startY,
    endX,
    endY,
  }: { startX: number; startY: number; endX: number; endY: number }
) {
  return new fabricInstance.Line([startX, startY, endX, endY], {
    fill: "#006eff",
    stroke: "#006eff",
    strokeWidth: 2,
    selectable: false,
    evented: false,
  });
}

function makePolygon(fabricInstance: any, canvasInstance: any) {
  function polygonPositionHandler(
    this: {
      positionHandler: (dim: any, finalMatrix: any, fabricObject: any) => any;
      actionHandler: (eventData: any, transform: any, x: any, y: any) => any;
      actionName: string;
      pointIndex: any;
    },
    dim: unknown,
    finalMatrix: unknown,
    fabricObject: any
  ) {
    const x =
      fabricObject.points[this.pointIndex].x - fabricObject.pathOffset.x;
    const y =
      fabricObject.points[this.pointIndex].y - fabricObject.pathOffset.y;
    return fabricInstance.current.util.transformPoint(
      { x, y },
      fabricInstance.current.util.multiplyTransformMatrices(
        fabricObject.canvas.viewportTransform,
        fabricObject.calcTransformMatrix()
      )
    );
  }

  function getObjectSizeWithStroke(object: any) {
    const stroke = new fabricInstance.current.Point(
      object.strokeUniform ? 1 / object.scaleX : 1,
      object.strokeUniform ? 1 / object.scaleY : 1
    ).multiply(object.strokeWidth);
    return new fabricInstance.current.Point(
      object.width + stroke.x,
      object.height + stroke.y
    );
  }
  function actionHandler(
    eventData: unknown,
    transform: any,
    x: number,
    y: number
  ) {
    if (x < 0 || y < 0) {
      x = Math.max(0, x);
      y = Math.max(0, y);
    }
    if (x > transform.target.canvas.width || y > transform.target.canvas.height) {
      x = Math.min(x, transform.target.canvas.width);
      y = Math.min(y, transform.target.canvas.height);
    }
    const polygon = transform.target;
    const currentControl = polygon.controls[polygon.__corner];
    const mouseLocalPosition = polygon.toLocalPoint(
      new fabricInstance.current.Point(x, y),
      "center",
      "center"
    );
    const polygonBaseSize = getObjectSizeWithStroke(polygon);
    const size = polygon._getTransformedDimensions(0, 0);
    const finalPointPosition = {
      x:
        (mouseLocalPosition.x * polygonBaseSize.x) / size.x +
        polygon.pathOffset.x,
      y:
        (mouseLocalPosition.y * polygonBaseSize.y) / size.y +
        polygon.pathOffset.y,
    };
    polygon.points[currentControl.pointIndex] = finalPointPosition;
    clearTimeout(changetimeout);
    changetimeout = setTimeout(() => {
      if (!canvasState) throw new Error("未知情况");

      const targetPolygonIndex = canvasState.polygonStack.findIndex(
        (item) => item.id === transform.target.id
      );
      const targetPolygon = canvasState.polygonStack[targetPolygonIndex];
      const prevPositions = targetPolygon.prevPositions.slice();
      if (
        checkPolygonHasIntersect(
          targetPolygon.points,
          currentControl.pointIndex
        )
      ) {
        canvasInstance.current.remove(targetPolygon);
        const prevPolygon = createPolygon(prevPositions, transform.target.id);
        prevPolygon.prevOffset = {
          left: prevPolygon.left,
          top: prevPolygon.top,
        };
        prevPolygon.prevPositions = deepClone(prevPositions);
        canvasState.polygonStack.splice(targetPolygonIndex, 1, prevPolygon);
        canvasInstance.current.add(prevPolygon);
        return;
      }
      canvasState.operateStack.push({
        type: OperateType.ChangePolygon,
        id: transform.target.id,
        prevPositions: deepClone(prevPositions),
        prevOffset: { ...targetPolygon.prevOffset },
      });
      targetPolygon.prevOffset = {
        left: targetPolygon.left,
        top: targetPolygon.top,
      };
      // 直接改变
      targetPolygon.prevPositions.splice(
        currentControl.pointIndex,
        1,
        finalPointPosition
      );
    }, 500);
    return true;
  }
  function anchorWrapper(anchorIndex: number, fn: any) {
    return function (eventData: any, transform: any, x: any, y: any) {
      const fabricObject = transform.target;
      const absolutePoint = fabricInstance.current.util.transformPoint(
        {
          x: fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x,
          y: fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y,
        },
        fabricObject.calcTransformMatrix()
      );
      const actionPerformed = fn(eventData, transform, x, y);
      fabricObject._setPositionDimensions({});
      const polygonBaseSize = getObjectSizeWithStroke(fabricObject);
      const newX =
        (fabricObject.points[anchorIndex].x - fabricObject.pathOffset.x) /
        polygonBaseSize.x;
      const newY =
        (fabricObject.points[anchorIndex].y - fabricObject.pathOffset.y) /
        polygonBaseSize.y;
      fabricObject.setPositionByOrigin(absolutePoint, newX + 0.5, newY + 0.5);
      return actionPerformed;
    };
  }

  const createPolygon = (points: any, id: number) => {
    const polygon = new fabricInstance.current.Polygon(points, {
      fill: "rgba(70, 134, 149, 0.4)",
      strokeWidth: 1,
      stroke: "#006eff",
      scaleX: 1,
      scaleY: 1,
      objectCaching: false,
      transparentCorners: false,
      selectable: true,
      hasBorders: false,
      cornerStyle: "circle",
      cornerColor: "#fff",
      cornerStrokeColor: "#006eff",
      id,
    });
    const lastControl = polygon.points.length - 1;
    polygon.controls = polygon.points.reduce(function (
      prev: any,
      point: any,
      index: any
    ) {
      prev["p" + index] = new fabricInstance.current.Control({
        positionHandler: polygonPositionHandler,
        actionHandler: anchorWrapper(
          index > 0 ? index - 1 : lastControl,
          actionHandler
        ),
        actionName: "modifyPolygon",
        pointIndex: index,
      });
      return prev;
    },
    {});
    return polygon;
  };
  return { createPolygon };
}
export function useLoadFabric({ canvasEl, dep, canvasSize }: any) {
  const [state, setState] = useState("load.....");
  const [editState, setEditState] = useState(false);
  const fabricInstance = useRef<any>();
  const canvasInstance = useRef<any>();
  const startLoad = () => {
    if ("fabric" in window) {
      return Promise.resolve(window.fabric);
    }
    return new Promise<any>((resolve, reject) => {
      LoadScript({
        id: "123",
        src: "https://cdn.bootcdn.net/ajax/libs/fabric.js/4.5.0/fabric.js",
        onload: () => {
          console.log("script onload");
          if ("fabric" in window) {
            resolve(window.fabric);
          }
        },
        onerror: () => {
          console.log("script error");
          reject();
        },
      });
    });
  };
  useLayoutEffect(() => {
    const [showDlg, canvasSize] = dep;
    if (showDlg && canvasSize.width && canvasSize.height) {
      canvasEl.current &&
        startLoad()
          .then((instance) => {
            fabricInstance.current = instance;
            canvasInstance.current = new instance.Canvas(canvasEl.current, {
              selection: false,
            });
            canvasInstance.current.setWidth(canvasSize?.width);
            canvasInstance.current.setHeight(canvasSize?.height);
            if (!canvasState) {
              canvasState = getInitCanvasState();
            } else {
              canvasInstance.current.add(...canvasState.polygonStack);
            }
            setState("success load");
          })
          .catch((err) => {
            console.log(err);
            setState("fail load");
          });
    } else {
      setEditState(false);
    }
  }, dep);
  useEffect(() => {
    if (state === "success load") {
      if (!canvasState) throw new Error("未知情况");
      if (editState) {
        if (canvasState.polygonStack.length) {
          canvasState.polygonStack.forEach((object: any) => {
            object.set("selectable", true);
          });
        }
        Object.keys(eventMap).forEach((item) => {
          if (
            item === "mouse:up" ||
            item === "mouse:up:before" ||
            item === "object:moving"
          ) {
            canvasInstance.current.on(
              item,
              eventMap[item](canvasInstance, fabricInstance)
            );
            return;
          }
          if (item === "mouse:move") {
            canvasInstance.current.on(item, eventMap[item](canvasInstance));
          }
        });
      } else {
        if (canvasState.polygonStack.length) {
          canvasInstance.current.discardActiveObject();
          canvasState.polygonStack.forEach((object: any) => {
            object.set("selectable", false);
          });
        }
        if (canvasInstance.current.__eventListeners) {
          Object.keys(canvasInstance.current.__eventListeners).forEach(
            (item) => {
              canvasInstance.current.__eventListeners[item].pop();
            }
          );
        }
      }
    }
  }, [state, editState]);
  const clearCanvasState = () => {
    canvasState = null;
  };
  useEffect(() => {
    return () => {
      clearCanvasState();
      canvasInstance.current && canvasInstance.current.removeListeners();
    };
  }, []);
  const updatePoygon = ({ detectArea }: { detectArea: DetectAreaItem[] }) => {
    if (!canvasState) throw new Error("未知情况");
    if (!detectArea) {
      console.log("没数据");
      return;
    }
    canvasState = getInitCanvasState();
    canvasInstance.current.clear();
    const { createPolygon } = makePolygon(fabricInstance, canvasInstance);
    const newPolygonArr = detectArea.map((item) => {
      const points = item.areaPoint.map(({ point: [x, y] }) => {
        return {
          x: x * canvasSize.width,
          y: y * canvasSize.height,
        };
      });
      const newPolygon = createPolygon(points, item.areaIndex - 1);
      newPolygon.selectable = false;
      newPolygon.prevOffset = { left: newPolygon.left, top: newPolygon.top };
      newPolygon.prevPositions = deepClone(points);
      return newPolygon;
    });
    canvasState.polygonStack = newPolygonArr;
    canvasInstance.current.add(...newPolygonArr);
  };
  const withdraw = () => {
    if (!canvasState) throw new Error("未知情况");

    if (!canvasState.operateStack.length) {
      console.log("----已经是初始状态了");
      return;
    }
    console.log("---撤回前", canvasState.operateStack.slice());
    const { createPolygon } = makePolygon(fabricInstance, canvasInstance);
    const { type, prevPositions, id, positions, prevOffset } =
      canvasState.operateStack.pop()!;
    const targetIndex = canvasState.polygonStack.findIndex(
      (item) => item.id === id
    );
    console.log(
      "---撤回中",
      deepClone({
        id,
        type,
        prevPositions,
        positions,
        prevOffset,
      })
    );
    // 撤回前的prevPositions
    switch (type) {
      case OperateType.AddNewLine:
        handleWithdrawLine();
        break;
      case OperateType.AddNewPolygon:
        handleWithdrawPolygon();
        break;
      case OperateType.ChangePolygon:
        handleWithdrawChangePolygon();
        break;
      case OperateType.DeletePolygon:
        handleWithdrawDeletePolygon();
        break;
      case OperateType.MovePolygon:
        handleWithdrawMovePolygon();
        break;
    }
    function handleWithdrawLine() {
      console.log("---撤回" + OperateType.AddNewLine);
      if (!canvasState) throw new Error("未知情况");

      canvasInstance.current.remove(
        canvasState.nodeStack[id],
        canvasState.lineStack[id]
      );
      canvasState.lineStack.pop();
      const withdrawNode = canvasState.nodeStack.pop();
      if (withdrawNode) {
        delete canvasState.nodePositions[withdrawNode.cleId];
      }
      canvasState.polygonStack.forEach((object: any) => {
        object.set('selectable', !canvasState!.nodeStack.length);
      });
    }
    function handleWithdrawPolygon() {
      console.log("---撤回" + OperateType.AddNewPolygon);
      if (!canvasState) throw new Error("未知情况");

      const { nodeStack, nodePositions, lineStack } =
        canvasState.taskStack.pop() || {};
      if (!nodeStack || !nodePositions || !lineStack) {
        console.log("----", "撤回队列为空");
        return;
      }
      canvasInstance.current.clear();
      canvasState.polygonStack.pop();
      canvasState.lineStack = lineStack;
      canvasState.nodePositions = nodePositions;
      canvasState.nodeStack = nodeStack;
      canvasState.polygonStack.forEach((object: any) => {
        object.set('selectable', false);
      });
      canvasInstance.current.add(
        ...lineStack,
        ...nodeStack,
        ...canvasState.polygonStack
      );
    }
    function handleWithdrawChangePolygon() {
      console.log("---撤回changegraph");
      if (!canvasState) throw new Error("未知情况");

      canvasInstance.current.remove(canvasState.polygonStack[targetIndex]);
      const prevPolygon = createPolygon(prevPositions, id);
      prevPolygon.prevOffset = prevOffset;
      prevPolygon.set("left", prevOffset!.left);
      prevPolygon.set("top", prevOffset!.top);
      prevPolygon.prevPositions = prevPositions!.slice();
      canvasState.polygonStack.splice(targetIndex, 1, prevPolygon);
      canvasInstance.current.add(prevPolygon);
    }

    function handleWithdrawDeletePolygon() {
      if (!canvasState) throw new Error("未知情况");

      const Polygon = createPolygon(positions, id);
      Polygon.prevOffset = prevOffset;
      Polygon.set("left", prevOffset!.left);
      Polygon.set("top", prevOffset!.top);
      Polygon.prevPositions = prevPositions!.slice();
      canvasState.polygonStack.splice(id, 0, Polygon);
      canvasInstance.current.add(Polygon);
    }
    function handleWithdrawMovePolygon() {
      if (!canvasState) throw new Error("未知情况");

      console.log(
        "---撤回",
        OperateType.MovePolygon,
        canvasState.polygonStack[targetIndex]
      );
      moveObject(
        canvasInstance,
        canvasState.polygonStack[targetIndex],
        prevOffset!
      );
      canvasState.polygonStack[targetIndex].prevOffset = prevOffset!;
    }
    console.log("---撤回后", canvasState.operateStack);
  };
  const deleteActivePolygon = () => {
    const { id } = canvasInstance.current.getActiveObject() ?? {};
    if (!id && id !== 0) {
      console.log("选中为空");
      return;
    }

    canvasInstance.current.clear();
    if (!canvasState) throw new Error("未知情况");
    canvasState.polygonStack = canvasState.polygonStack.filter((item) => {
      if (item.id === id) {
        canvasState!.operateStack.push({
          type: OperateType.DeletePolygon,
          positions: item.points,
          prevPositions: item.prevPositions,
          prevOffset: { left: item.left, top: item.top },
          id,
        });
        return false;
      }
      return true;
    });
    canvasInstance.current.add(...canvasState.polygonStack);
  };
  const getALLarea = () => {
    if (!canvasState) throw new Error("未知情况");

    return canvasState.polygonStack.map((polygon: any) => {
      return {
        points: Object.values(polygon.oCoords).map((item: any) => {
          return {
            x: item.x,
            y: item.y,
          };
        }),
      };
    });
  };
  return {
    state,
    editState,
    setEditState,
    withdraw,
    getALLarea,
    deleteActivePolygon,
    clearCanvasState,
    updatePoygon,
  };
}
interface LineNode {
  startPointX: number;
  startPointY: number;
  endPointX: number;
  endPointY: number;
}
function isIntersect(firstNode: LineNode, secondNode: LineNode) {
  // 判断是否相交算法
  let flag = false;
  const d =
    (firstNode.endPointX - firstNode.startPointX) *
      (secondNode.endPointY - secondNode.startPointY) -
    (firstNode.endPointY - firstNode.startPointY) *
      (secondNode.endPointX - secondNode.startPointX);
  if (d != 0) {
    const r =
      ((firstNode.startPointY - secondNode.startPointY) *
        (secondNode.endPointX - secondNode.startPointX) -
        (firstNode.startPointX - secondNode.startPointX) *
          (secondNode.endPointY - secondNode.startPointY)) /
      d;
    const s =
      ((firstNode.startPointY - secondNode.startPointY) *
        (firstNode.endPointX - firstNode.startPointX) -
        (firstNode.startPointX - secondNode.startPointX) *
          (firstNode.endPointY - firstNode.startPointY)) /
      d;
    if (r >= 0 && r <= 1 && s >= 0 && s <= 1) {
      flag = true;
    }
  }
  return flag;
}
function formatLineToPoint(line: Line, offset = 0) {
  const { x1, x2, y1, y2 } = line;
  return {
    startPointX: x1 - offset,
    startPointY: y1 - offset,
    endPointX: x2 - offset,
    endPointY: y2 - offset,
  };
}
function checkLineHasIntersect(lineArr: Line[]) {
  const currentLineIndex = lineArr.length - 1;
  // 最后一条的前一条不计算相交
  let needCheckEnd = currentLineIndex - 2;
  let needCheckStart = 0;
  if (currentLineIndex < 2) {
    return false;
  }
  while (needCheckStart <= needCheckEnd) {
    if (needCheckStart === needCheckEnd) {
      return isIntersect(
        formatLineToPoint(lineArr[needCheckStart], 0.5),
        formatLineToPoint(lineArr[currentLineIndex])
      );
    }
    const startHasIns = isIntersect(
      formatLineToPoint(lineArr[needCheckStart], 0.5),
      formatLineToPoint(lineArr[currentLineIndex])
    );
    const endHasIns = isIntersect(
      formatLineToPoint(lineArr[needCheckEnd], 0.5),
      formatLineToPoint(lineArr[currentLineIndex])
    );
    if (startHasIns || endHasIns) {
      return true;
    }
    needCheckStart += 1;
    needCheckEnd -= 1;
  }
  return false;
}
function checkLineIntersectPolygon(
  polygonLine: any,
  currentLineIndex: number,
  notcheckIndexArr: number[]
) {
  let needCheckStart = 0;
  let needCheckEnd = polygonLine.length - 1;
  if (currentLineIndex === needCheckEnd) {
    notcheckIndexArr.push(0);
  }
  while (needCheckStart <= needCheckEnd) {
    if (notcheckIndexArr.includes(needCheckStart)) {
      needCheckStart += 1;
      continue;
    }
    if (notcheckIndexArr.includes(needCheckEnd)) {
      needCheckEnd -= 1;
      continue;
    }
    if (needCheckStart === needCheckEnd) {
      return isIntersect(
        formatLineToPoint(polygonLine[needCheckStart], 0.5),
        formatLineToPoint(polygonLine[currentLineIndex])
      );
    }
    const startHasIns = isIntersect(
      formatLineToPoint(polygonLine[needCheckStart], 0.5),
      formatLineToPoint(polygonLine[currentLineIndex])
    );
    const endHasIns = isIntersect(
      formatLineToPoint(polygonLine[needCheckEnd], 0.5),
      formatLineToPoint(polygonLine[currentLineIndex])
    );
    if (startHasIns || endHasIns) {
      return true;
    }
    needCheckStart += 1;
    needCheckEnd -= 1;
  }
}
function checkPolygonHasIntersect(points: any, moveIndex: number) {
  const polygonLine = points.reduce(
    (prev: any, next: any, index: number, self: any) => {
      const isLast = index === self.length - 1;
      return prev.concat({
        x1: next.x,
        y1: next.y,
        x2: !isLast ? self[index + 1].x : self[0].x,
        y2: !isLast ? self[index + 1].y : self[0].y,
      });
    },
    []
  );

  let prevIndex = moveIndex - 1;
  let prevNotCheckIndexs = prevIndex
    ? [moveIndex, prevIndex, prevIndex - 1]
    : [moveIndex, prevIndex, polygonLine.length - 1];
  const curNotCheckIndexs = moveIndex
    ? [prevIndex, moveIndex, moveIndex + 1]
    : [polygonLine.length - 1, moveIndex, moveIndex + 1];
  if (!~prevIndex) {
    prevIndex = polygonLine.length - 1;
    prevNotCheckIndexs = [0, polygonLine.length - 1, polygonLine.length - 2];
  }
  const prevHasIntersect = checkLineIntersectPolygon(
    polygonLine,
    prevIndex,
    prevNotCheckIndexs
  );
  const curHasIntersect = checkLineIntersectPolygon(
    polygonLine,
    moveIndex,
    curNotCheckIndexs
  );
  if (prevHasIntersect) {
    console.log("prevHasIntersect");
  }
  if (curHasIntersect) {
    console.log("curHasIntersect");
  }
  return prevHasIntersect || curHasIntersect;
}
