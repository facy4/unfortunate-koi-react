import React, { createContext, useCallback } from 'react';
import { Route, Routes, useNavigate } from "react-router-dom";
import { Test1 } from "./views";
export const GlobalContext = createContext<{ goPath: (path: string, options?: { [key: string]: unknown } ) => void } | null>(null);
import './app.scss';
import { NavBar } from './commponts/NavBar';
function App() {
  const navigate = useNavigate();
  const goPath = useCallback((path: string, options?: { [key: string]: unknown }) => {
    navigate(path, options);
  }, []);
  return (
    <GlobalContext.Provider
      value={{
        goPath
      }}>
      <section className='app'>
        <section className='header'>
          <NavBar></NavBar>
        </section>
        <section className='main'>
          <Routes>
            <Route path='test' element={<Test1></Test1>} >
            </Route>
          </Routes>
        </section>
        <section className='footer'></section>
      </section>
    </GlobalContext.Provider>
  );
}

export default App;
