import React, { useContext } from 'react';
import { GlobalContext } from '../App';
import { MenuConfigs } from '../constants/menu';
export function NavBar() {
  const { goPath } = useContext(GlobalContext) || {};
  return <div>
    <h1 className='text-center'>unfortunate koi</h1>
    {
      Object.values(MenuConfigs).map(item => {
        return <button key={item.id} onClick={() => goPath && goPath(item.path)}>{item.title}</button>
      })
    }
  </div>
}