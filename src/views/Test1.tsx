import React, { useCallback, useRef, useState } from "react";
import { useLoadFabric } from "../hook";
export function Test1() {
  const canvasEl = useRef(null);
  const [canvasSize, setCanvasSize] = useState({
    width: 0,
    height: 0,
  });
  const [showDlg, setShowDlg] = useState(false);
  const updateAccumulation = async (params: unknown) => {
    const headerOptions: {
      Authentication: string;
      'Content-Type': string
    } = {
      Authentication: '',
      'Content-Type': 'application/json;charset=utf-8',
    }
    const headers = new Headers(Object.assign({}, headerOptions));
    const initOptions = {
      headers,
      method: 'POST',
      body: JSON.stringify(params),
    };
    const request = new Request('/api/accumulation/updateAccumulationContent', initOptions);
    const fetchResponse = await fetch(request);
    if (!fetchResponse.ok) {
      return Promise.reject(fetchResponse.statusText);
    }
    const fetchResponseJson = await fetchResponse.json();
    console.log(fetchResponseJson);
  }

  const measuredRef = useCallback((node: HTMLElement | null) => {
    if (node !== null) {
      setCanvasSize({
        width: node.clientWidth,
        height: 470,
      });
    }
  }, []);
  const { state, editState, setEditState, withdraw, getALLarea, deleteActivePolygon, updatePoygon } = useLoadFabric({
    canvasEl,
    dep: [showDlg, canvasSize],
    canvasSize,
  });
  const refresh = async () => {
    const { data: { articleDetail: { content } } } = await (await fetch('/api/accumulation/getAccumulation?id=17')).json()
    try {
      updatePoygon(JSON.parse(content))
      setEditState(false)
    } catch (err) {
      console.log(err);

    }
  }
  const onConfirm = () => {
    const contentStr = {
      detectArea: getALLarea().map((item, index) => ({
        areaPoint: item.points.map(({ x, y }: { x: number, y: number }) => {
          return {
            point: [x / canvasSize.width, y / canvasSize.height]
          };
        }),
        areaIndex: index + 1
      }))
    }
    updateAccumulation({
      content: JSON.stringify(contentStr),
      id: 17,
      title: 'this is fabric content container'
    })
  }
  return <>
    <h3>状态：{state}</h3>
    <h3>{editState ? '编辑模式' : '只读模式'}</h3>
    <h3>点击开启进入编辑模式即可状态为success load 即可进入编辑模式</h3>
    <button onClick={refresh}>刷新</button>
    <button onClick={() => setShowDlg(val => !val)}>{showDlg ? '关闭' : '开启'}</button>
    {!editState ? <button disabled={state !== 'success load'} onClick={() => { setEditState(true) }}>编辑</button> : <>
      <button onClick={deleteActivePolygon}>删除区域</button>
      <button onClick={withdraw}>撤回</button>
      <button onClick={getALLarea}>获取点信息</button>
      <button onClick={onConfirm}>保存</button>
      <button onClick={() => { setEditState(false) }}>取消编辑</button>
    </>}
    {showDlg && <div ref={measuredRef}><canvas width="700" height="700" style={{ background: 'transparent' }} ref={canvasEl} /></div>}
  </>
}
