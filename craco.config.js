const CompressionPlugin = require("compression-webpack-plugin");

const isProd = process.env.NODE_ENV === "production";
const { whenProd, getPlugin, pluginByName } = require("@craco/craco");
const loadPlugins = () => {
  const plugins = [];
  if (isProd) {
    plugins.push(
      new CompressionPlugin({
        algorithm: "gzip", // 使用gzip压缩
        test: /\.js$|\.html$|\.css$/, // 匹配文件名
        filename: "[path].gz[query]", // 压缩后的文件名(保持原文件名，后缀加.gz)
        minRatio: 1, // 压缩率小于1才会压缩
        threshold: 10240, // 对超过10k的数据压缩
        deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
      })
    );
  }
};
module.exports = {
  webpack: {
    configure: (webpackConfig) => {
      let cdn = {
        js: [],
        css: [],
      };
      whenProd(() => {
        webpackConfig.plugins = Object.assign(
          webpackConfig.plugins,
          loadPlugins()
        );
        webpackConfig.externals = {
          react: "React",
          "react-dom": "ReactDOM",
        };
        cdn = {
          js: [
            "https://unpkg.com/react@18/umd/react.production.min.js",
            "https://unpkg.com/react-dom@18/umd/react-dom.production.min.js",
          ],
          css: [],
        };
        const { isFound, match } = getPlugin(
          webpackConfig,
          pluginByName("HtmlWebpackPlugin")
        );
        if (isFound) {
          match.userOptions.cdn = cdn;
        }
      });
      return webpackConfig;
    },
  },
};
